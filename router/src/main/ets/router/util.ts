export function merge(p1, p2): ESObject {
  return { ...(p1 ?? {}), ...(p2 ?? {}) }
}

export function callMethod(instance: ESObject, methodName: string, ...args: ESObject[]): ESObject | undefined {
  return instance[methodName](...args);
}