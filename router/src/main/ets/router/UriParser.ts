import { uri } from '@kit.ArkTS';

export default class UriParser {
  static parse(uriStr: string): UriOptions {
    return this.parseUri(new uri.URI(uriStr));
  }

  static parseUri(uri: uri.URI): UriOptions {
    return {
      scheme: uri.scheme,
      host: uri.host,
      path: uri.path,
      queryParameters: this.parseQuery(uri.query),
    }
  }

  private static parseQuery(query: string): QueryParameters {
    if (!query) {
      return {};
    }

    const paramPart = query.split('&');
    return paramPart.reduce(function (res, item) {
      const parts = item.split('=');
      res[parts[0]] = parts[1];
      return res;
    }, {});
  }
}

export interface UriOptions {
  scheme: string;
  host: string;
  path?: string;
  queryParameters?: QueryParameters;
}

export interface QueryParameters {
  [key: string]:
    | undefined
    | string
    | string[]
    | boolean
    | boolean[]
    | number
    | number[]
    | QueryParameters
    | QueryParameters[];
}

