import { harTasks } from '@ohos/hvigor-ohos-plugin';
import { RouterLibraryPlugin } from '../../router_plugin'

export default {
  system: harTasks,
  plugins: [
    new RouterLibraryPlugin()
  ]
}
