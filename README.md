# NlNavigation
一款用于harmonyOS的动态化路由的库，基于组件化思维，功能灵活，使用简单。


## 功能介绍
框架主要提供Router和ServiceLoader两大功能。

Router主要使用标准的uri进行跳转，使用方便，可以和android，ios打通，还拓展了NavStackPath的部分功能。

1. 支持通过设置scheme，host，path，regex匹配路由。
2. 支持注解（方法装饰器）的方式自动化注册路由。
3. 注册表为代码实现，不会有启动io耗时。
4. 支持全局以及单个页面拦截器，可以实现更加自由的功能。
5. 支持跳转监听以及降级策略。

Service使用类似ServiceLoader的思想实现。

1. 使用注解注册Service
2. 调用简单，通过字符串协议调用方法，无需硬编码。

## 使用文档

下载router-${lastVersion}.har以及router_plugin-${lastVersion}.har (暂时没有发送到ohpm)

### 根目录下
hvigor/hvigor-config.json5 添加插件
```json5
{
  "dependencies": {
    "@newlink/navigation-plugin": "file:../plugins/router_plugin.tgz",
  }
}

```

.gitignore下添加

```gitignore
**/src/main/ets/generated
```

### 业务模块
oh-package.json添加依赖
```json5
{
    "dependencies": {
        "@newlink/navigation": "file:./router.har",
    }
}
```

hvigorfile.ts添加插件
```javascript
import { harTasks } from '@ohos/hvigor-ohos-plugin';
import { RouterLibraryPlugin } from 'router_plugin'

export default {
    system: harTasks,
    plugins: [
        new RouterLibraryPlugin()
    ]
}
```

添加路由RouterUri
```typescript
import { NavParams, RouterUri, SingleTopInterceptor } from '@newlink/navigation';

// options 如果需要获取传递过来的参数，请使用options，如果不需要可删除{options: new NavOptions(params)}
@Builder
export function providerFeature1PageOne(params: NavParams) {
  Feature1PageOne({options: new NavOptions(params)})
}

// 路由
@RouterUri({
  // 必填
  builder: providerFeature1PageOne,
  // 根据情况填写
  host: "feature1",
  path: "/PageOne",
  // 或者
  regex: /^https?:\/\//,
  // 拦截器（可自定义）
  interceptors: [SingleTopInterceptor],
})
@Component
export struct Feature1PageOne {

  options?: NavOptions;
  
  build() {
    NavDestination() {
      Row() {
        Text('Feature1PageOne')
      }.justifyContent(FlexAlign.Center)
      .width('100%')
      .height('100%')
    }.title("Feature1PageOne")
  }
}
```

添加RouterService

```typescript

import { RouterService } from '@newlink/navigation';

// 必填
@RouterService('Feature2Service')
export default class Feature2Service {
  // callFunction的时候填写方法名
  add(a: number, b: number): number {
    return a + b;
  }
}

```

编写后rebuild会在当前模块 src/main/ets/generated下生成以下内容，并检查build/${productName}/nlrouter/router_map.json是否存在，两项没问题表示成功

```typescript
import {
  providerNameNavigationAction,
  RegexMatcher,
  PathMatcher,
  PageUriChildHandler,
  ModuleRegisterUriHandler,
} from '@newlink/navigation';
import { providerFeature2PageFive } from '../pages/Feature2PageFive';
import { SingleTopInterceptor } from '@newlink/navigation';
import Feature2Service from '../Feature2Service';

export default class ModuleUriHandler extends ModuleRegisterUriHandler {
  constructor(defaultScheme: string) {
    super();
    // 注册RouterHandler
    this.addChildHandler(new PageUriChildHandler(defaultScheme, 
      providerNameNavigationAction('Feature2PageFive', new PathMatcher({host: 'feature2', path: '/Feature2PageFive'}), wrapBuilder(providerFeature2PageFive), [
      new SingleTopInterceptor()
    ])));
    // 注册ServiceHandler
    this.addServiceHandler('Feature2Service', new Feature2Service());
 }
}

```

### entry模块

oh-package.json添加依赖
```json5
{
    "dependencies": {
        "@newlink/navigation": "file:./router.har",
    }
}
```

hvigorfile.ts添加插件
```javascript
import { hapTasks } from '@ohos/hvigor-ohos-plugin';
import { RouterAppPlugin } from 'router_plugin'
import { hvigor } from '@ohos/hvigor';


export default {
  system: hapTasks,
  plugins: [
    new RouterAppPlugin(hvigor)
  ]
}

```

rebuild后，会在src/main/ets/generated下生成以下内容表示成功：
```typescript
import ConfigUriHandler from '@newlink/navigation/src/main/ets/router/ConfigUriHandler';
import EntryModuleUriHandler from './ModuleUriHandler';
import FeaturesFeature1ModuleUriHandler from '@features/feature1/src/main/ets/generated/ModuleUriHandler';
import FeaturesFeature2ModuleUriHandler from '@features/feature2/src/main/ets/generated/ModuleUriHandler';

// 用来总体注册所有模块的Handler
export default class EntryConfigUriHandler extends ConfigUriHandler {
  constructor(defaultScheme: string) {
    super();
    this.addChildHandler(new EntryModuleUriHandler(defaultScheme));
    this.addChildHandler(new FeaturesFeature1ModuleUriHandler(defaultScheme));
    this.addChildHandler(new FeaturesFeature2ModuleUriHandler(defaultScheme));
 }
}
```

初始化：
```typescript
import UIAbility from '@ohos.app.ability.UIAbility';
import window from '@ohos.window';
import { NlNavigator, DefaultRootUriHandler } from '@newlink/navigation';
import EntryUriHandlerBuilder from '../generated/EntryConfigUriHandler';
import LoginInterceptor from './LoginInterceptor';

export default class EntryAbility extends UIAbility {
  onCreate() {
    const handler = new DefaultRootUriHandler(this.context);
    // 注意这个EntryConfigUriHandler是刚才生成的类
    // defaultScheme必填
    handler.addChildHandler(new EntryUriHandlerBuilder("nlsample://"));
    // 添加全局拦截器
    handler.addInterceptor(new LoginInterceptor());
    NlNavigator.init(handler);
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    windowStage.loadContent('pages/Index');
  }
}
```

首页Navigation
```typescript
@Entry
@Component
struct Index {
  private navPathStack = NlNavigator.getNavPathStack();

  aboutToAppear(): void {
    this.navPathStack.replacePath({ name: 'EntryMainPage' })
  }

  // 首页也放到NavPathStack中管理
  @Builder
  providerNavDestination(name: string, params: object) {
    if (name == 'EntryMainPage') {
      MainPage()
    }
    findNavDestinationWrapperBuilder(name)?.builder(params);
  }

  build() {
    Navigation(this.navPathStack) {
      Stack()
    }.hideTitleBar(true)
    .mode(NavigationMode.Stack)
    .expandSafeArea([SafeAreaType.SYSTEM])
    .navDestination(this.providerNavDestination)
  }
}

@Component
struct MainPage {
  build() {
    NavDestination() {
      ...
    }.onBackPressed(() => {
      // 这里不加返回会空白
      return DoubleClickBackUtil.backForward(this.context!);
    })
  }
}

```

以上初始化就完成了。

### 跳转

```typescript
// 支持标准uri，可以写带参数，不携带可不填
NlNavigator.pushUri({
  uri: new uri.URI("nlsample://feature2/Feature2PageFour?needLogin=true"),
  onResult: (result) => {
    // 上一个页面Pop的结果
  }
});
```

### Service方法调用
```typescript
const result = NlNavigator.getService("Feature2Service")?.callFunc<number>('add', 1, 2);
```

### 其他方法
```typescript
// pop 并返回结果
NlNavigator.pop('123');
// pop 到第几个页面
NlNavigator.popTo(index);
// 删除pageId的对应的页面
NlNavigator.popSelf(pageId);
// 从pageId开始向下关闭几个页面
NlNavigator.popSection(pageId, count);


// NavOptions
options: NavOptions;
// 当前页面id，用来栈管理
options.getPageId(id);
// 获得uri上面的参数，都是string需要根据需求转换
options.getQueryParameters();
// 获取原始uri
options.getData();
// 获取Uri对象
options.getUri();


```