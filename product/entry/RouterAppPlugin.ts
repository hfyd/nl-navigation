
import { hvigor, HvigorNode, HvigorPlugin } from '@ohos/hvigor';
import { OhosHapContext, OhosPluginId, Target } from '@ohos/hvigor-ohos-plugin';


export function RouterAppPlugin(): HvigorPlugin {
  return {
    pluginId: 'NlRouterAppPlugin',
    apply: async (node: HvigorNode) => {
      console.log('hello NlRouterAppPlugin1!');
      hvigor.nodesEvaluated(() => {
        console.log('nodesEvaluated');
        hapTask(node);
      })
    }
  }
}

function hapTask(currentNode: HvigorNode) {
  // 等待全部节点加载完成之后获取子节点信息
  console.log('hapTask:');
  hvigor.getRootNode().subNodes((node: HvigorNode) => {

    // 获取hap模块上下文信息
    const hapContext = node.getContext(OhosPluginId.OHOS_HAP_PLUGIN) as OhosHapContext;
    const moduleName = hapContext?.getModuleName();
    console.log('subNodes:', hapContext?.getOhpmDependencyInfo());
    hapContext?.targets((target: Target) => {
      const targetName = target.getTargetName();
      console.log('targetName:',targetName);
      const outputPath = target.getBuildTargetOutputPath();
      // 禁用任务
      node.getTaskByName(`${target.getTargetName()}@SignHap`)?.setEnable(false);
      node.registerTask({
        // 任务名称
        name: `${targetName}@onlineSignHap`,
        // 任务执行逻辑主体函数
        run() {
          console.log('module Task');
        },
        // 配置前置任务依赖
        dependencies: [`${targetName}@PackageHap`],
        // 配置任务的后置任务依赖
        postDependencies: ['assembleHap']
      });
    });
  });
}
