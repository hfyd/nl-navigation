// Script for compiling build behavior. It is built in the build plug-in and cannot be modified currently.
import { hapTasks } from '@ohos/hvigor-ohos-plugin';
import { RouterAppPlugin } from '../../router_plugin'
import { hvigor } from '@ohos/hvigor';


export default {
  system: hapTasks,
  plugins: [
    new RouterAppPlugin(hvigor)
  ]
}

