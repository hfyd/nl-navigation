import * as fs from "fs";
import Handlebars from "handlebars";
import FileUtils from "../utils/FileUtils";
import {
  OhosModuleContext,
  Target,
} from "@ohos/hvigor-ohos-plugin/src/plugin/context/plugin-context";
import { ImportsDescriptor } from "./interfaces";
import path from "path";

export function generateEntryUriHandler(
  target: Target,
  context: OhosModuleContext
) {
  const rootPath = context.getModulePath();
  const registers = findModuleRegisters(target, context);
  if (registers.length == 0) {
    return;
  }

  const generatedDir = `${rootPath}/src/main/ets/generated`;
  const generatorFilePath = `${generatedDir}/EntryConfigUriHandler.ets`;
  FileUtils.mkDir(generatedDir);
  generate(generatorFilePath, registers);
}

function generate(generatorFilePath, registers: ImportsDescriptor[]) {
  const templateTxt = __dirname + "/templates/EntryUriHandlerBuilder.hbs";
  const output = Handlebars.compile(
    fs.readFileSync(templateTxt, { encoding: "utf8" })
  )({ imports: registers });

  fs.writeFileSync(generatorFilePath, output, { encoding: "utf8" });
}

function toCamelCase(str) {
  const cleaned = str.replace(/[^a-zA-Z0-9]/g, " ");
  return cleaned
    .trim()
    .split(/\s+/)
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
    .join("");
}

function findModuleRegisters(
  target: Target,
  context: OhosModuleContext
): ImportsDescriptor[] {
  const depOps = context.getOhpmDependencyInfo();
  const rootPath = context.getModulePath();
  const moduleResister: ImportsDescriptor[] = [];
  // entry module
  const entryFilePath = `${rootPath}/build/${target
    .getCurrentProduct()
    .getProductName()}/nlrouter/router_map.json`;
  if (fs.existsSync(entryFilePath)) {
    const className = `ModuleUriHandler`;
    moduleResister.push({
      name: `${toCamelCase(context.getModuleName()) + className}`,
      path: `./${className}`,
    });
  }

  // child module
  Object.keys(depOps).forEach((name) => {
    const packagePath = depOps[name].packagePath;
    const childFilePath = `${packagePath}/build/${target
      .getCurrentProduct()
      .getProductName()}/nlrouter/router_map.json`;
    if (fs.existsSync(childFilePath)) {
      moduleResister.push({
        name: `${toCamelCase(name)}ModuleUriHandler`,
        path: `${name}/src/main/ets/generated/ModuleUriHandler`,
      });
    }
  });

  return moduleResister;
}

function getImportPath(
  rootPath: string,
  importPath: string,
  filePath: string = "src/main/ets/generated"
): string {
  if (!importPath.startsWith(".")) {
    // node_modules
    return importPath;
  }

  const profilePath = path.resolve(rootPath, importPath);
  const generatedPath = path.resolve(rootPath, filePath);
  const relativePath = path.relative(generatedPath, profilePath);
  return relativePath.replace(/\.[^/.]+$/, "");
}
