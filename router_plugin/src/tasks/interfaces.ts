export interface RouterMapJSON {
  name: string;
  routerMap: RouterMap[];
}

export interface RouterMap {
  name?: string;
  pageSourceFile?: string;
  buildFunction?: string;
  ohmurl?: string;
  bundleName?: string;
  moduleName?: string;
  absolutePath?: string;
}

export interface LoaderJson {
  routerMap?: RouterMap[];
  compileEntry?: string[];
  modulePathMap?: Record<string, string>;
}

export interface PageDescriptor {
  name: string;
  builder?: ImportsDescriptor;
  scheme?: {
    scheme?: string;
    host?: string;
    path?: string;
  };
  regex?: string;
  interceptors?: ImportsDescriptor[];
}

export interface RouterDescriptor {
  pages: PageDescriptor[];
  services: FuncDescriptor[];
}

export interface FuncDescriptor {
  name?: string;
  imports?: ImportsDescriptor;
}

export interface ImportsDescriptor {
  isDefault?: boolean;
  name?: string;
  path?: string;
}