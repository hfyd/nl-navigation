import ts, { isDebuggerStatement, SourceFile } from "typescript";
import FileUtils from "../utils/FileUtils";
import path from "path";
import {
  FuncDescriptor,
  ImportsDescriptor,
  PageDescriptor,
  RouterDescriptor,
} from "./interfaces";

interface Source {
  filePath: string;
  sourceFile: SourceFile;
}

export function parseRouterDescriptorByDecorator(
  projectRootPath: string
): RouterDescriptor | null {
  const scanPath = `${projectRootPath}/src/main/ets`;

  const etsFile = FileUtils.walkSync(scanPath, /\.ets$/);
  const sourceFiles = etsFile.map(createTsSource);

  const routerUri = sourceFiles.filter((source) =>
    findDecorator("RouterUri", source)
  );
  const routerService = sourceFiles.filter((source) =>
    findDecorator("RouterService", source)
  );

  if (routerUri.length == 0 && routerService.length == 0) {
    return null;
  }

  return {
    pages:
      routerUri.map((source) => mapperToPages(source, projectRootPath)) ?? [],
    services:
      routerService.map((source) =>
        mapperToServices(source, projectRootPath)
      ) ?? [],
  };
}

function mapperToPages(source: Source, rootPath: string): PageDescriptor {
  const sourceFile = source.sourceFile;
  const filePath = source.filePath;
  const descriptor: PageDescriptor = { name: FileUtils.getFileName(filePath) };
  const imports = getImportsFromSourceFile(sourceFile, rootPath, filePath);
  visitDecorator("RouterUri", sourceFile, (node: ts.Node) => {
    visitProperties("builder", node, (builderNode: ts.PropertyAssignment) => {
      setBuilder(descriptor, builderNode, imports);
    });
    visitProperties("name", node, (hostNode: ts.PropertyAssignment) => {
      setName(descriptor, hostNode);
    });
    visitProperties("host", node, (hostNode: ts.PropertyAssignment) => {
      setScheme(descriptor, "host", hostNode);
    });
    visitProperties("path", node, (pathNode: ts.PropertyAssignment) => {
      setScheme(descriptor, "path", pathNode);
    });
    visitProperties("scheme", node, (schemeNode: ts.PropertyAssignment) => {
      setScheme(descriptor, "scheme", schemeNode);
    });
    visitProperties("regex", node, (regexNode: ts.PropertyAssignment) => {
      setRegex(descriptor, regexNode);
    });
    visitProperties(
      "interceptors",
      node,
      (interceptorNode: ts.PropertyAssignment) => {
        setInterceptors(descriptor, interceptorNode, imports);
      }
    );
  });

  return descriptor;
}

function mapperToServices(source: Source, rootPath: string): FuncDescriptor {
  const descriptor: FuncDescriptor = {};

  const filePath = source.filePath;
  const sourceFile = source.sourceFile;
  const imports = getImportsFromSourceFile(sourceFile, rootPath, filePath);

  visitDecorator("RouterService", sourceFile, (node: ts.Node) => {
    visitServiceName(node, (funcNode: ts.StringLiteral) => {
      descriptor.name = funcNode.text;
      descriptor.imports = {
        ...imports[0],
        isDefault: true,
        name: FileUtils.getFileName(filePath),
      };
    });
  });

  return descriptor;
}

function visitServiceName(node: ts.Node, visit: (node: ts.StringLiteral) => void) {
  if (
    ts.isCallExpression(node) &&
    node.arguments?.length == 1 &&
    ts.isStringLiteral(node.arguments[0])
  ) {
    visit(node.arguments[0]);
  }
}

function setBuilder(
  descriptor: PageDescriptor,
  node: ts.PropertyAssignment,
  imports: ImportsDescriptor[]
) {
  if (
    ts.isIdentifier(node.initializer) &&
    ts.isIdentifier(node.name) &&
    node.name.escapedText == "builder" &&
    node.initializer.escapedText
  ) {
    const escapedText = node.initializer.escapedText;
    descriptor.builder = imports.find(
      (importDesc) => importDesc.name == escapedText
    ) ?? {
      name: escapedText,
      path: imports[0].path,
      isDefault: false,
    };
  }
}

function setInterceptors(
  descriptor: PageDescriptor,
  node: ts.PropertyAssignment,
  imports: ImportsDescriptor[]
) {
  if (
    ts.isArrayLiteralExpression(node.initializer) &&
    node.initializer.elements.length > 0
  ) {
    const identifiers = node.initializer.elements
      .map((element) => {
        if (ts.isIdentifier(element)) {
          return element.text;
        }
        return null;
      })
      .filter((id) => id !== null);

    descriptor.interceptors = imports.filter((importDesc) =>
      identifiers.includes(importDesc.name!)
    );
  }
}

function getImportsFromSourceFile(
  sourceFile: SourceFile,
  rootPath: string,
  filePath: string
): ImportsDescriptor[] {
  const imports: ImportsDescriptor[] = [];
  imports.push({
    name: "$__current_file_path__$",
    isDefault: false,
    path: `./${path.relative(rootPath, filePath)}`,
  });
  ts.forEachChild(sourceFile, function visit(node) {
    if (ts.isImportDeclaration(node)) {
      const importPath = node.moduleSpecifier.getText().replace(/['"]/g, "");
      const namedBindings = node.importClause?.namedBindings;

      if (namedBindings && ts.isNamedImports(namedBindings)) {
        namedBindings.elements.forEach((element) => {
          imports.push({
            isDefault: false,
            name: element.name.getText(),
            path: resolveRootPath(rootPath, importPath, filePath),
          });
        });
      }

      if (node.importClause?.name) {
        imports.push({
          isDefault: true,
          name: node.importClause.name.getText(),
          path: resolveRootPath(rootPath, importPath, filePath),
        });
      }
    }

    ts.forEachChild(node, visit);
  });

  return imports;
}

function resolveRootPath(
  rootPath: string,
  importPath: string,
  filePath: string
): string {
  if (importPath.startsWith(".")) {
    return `./${path.relative(
      rootPath,
      path.resolve(path.dirname(filePath), importPath)
    )}`;
  }
  return importPath;
}

function setName(descriptor: PageDescriptor, node: ts.PropertyAssignment) {
  if (ts.isStringLiteral(node.initializer) && node.initializer.text) {
    if (!descriptor.scheme) {
      descriptor.scheme = {};
    }

    descriptor.name = node.initializer.text;
  }
}

function setRegex(descriptor: PageDescriptor, node: ts.PropertyAssignment) {
  if (
    ts.isRegularExpressionLiteral(node.initializer) &&
    node.initializer.text
  ) {
    descriptor.regex = node.initializer.text;
  }
}

function setScheme(
  descriptor: PageDescriptor,
  key: string,
  node: ts.PropertyAssignment
) {
  if (ts.isStringLiteral(node.initializer) && node.initializer.text) {
    if (!descriptor.scheme) {
      descriptor.scheme = {};
    }

    descriptor.scheme[key] = node.initializer.text;
  }
}

function createTsSource(filePath): Source {
  const sourceCode = ts.sys.readFile(filePath);
  if (!sourceCode) {
    throw new Error(`Cannot read file: ${filePath}`);
  }

  return {
    sourceFile: ts.createSourceFile(
      filePath,
      sourceCode,
      ts.ScriptTarget.ESNext,
      true
    ),
    filePath,
  };
}

function visitProperties(
  name: string,
  targetNode: ts.Node,
  visitFn: (node: ts.PropertyAssignment) => void
) {
  function visit(node: ts.Node) {
    if (
      ts.isPropertyAssignment(node) &&
      ts.isIdentifier(node.name) &&
      node.name.escapedText === name
    ) {
      visitFn(node);
    }
    ts.forEachChild(node, visit);
  }

  visit(targetNode);
}

function visitDecorator(
  name: string,
  sourceFile: SourceFile,
  visitFn: (node: ts.Node) => void
) {
  function visit(node: ts.Node) {
    if (
      ts.isCallExpression(node) &&
      ts.isIdentifier(node.expression) &&
      node.expression.escapedText === name
    ) {
      visitFn(node);
    }
    ts.forEachChild(node, visit);
  }

  visit(sourceFile);
}

function findDecorator(name: string, source: Source) {
  let hasRouterDecorator = false;
  visitDecorator(name, source.sourceFile, () => (hasRouterDecorator = true));
  return hasRouterDecorator;
}
