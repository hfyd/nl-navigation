import { HvigorNode, HvigorTask } from "@ohos/hvigor";
import { Target, OhosPluginId, OhosHapContext } from "@ohos/hvigor-ohos-plugin";
import { generateModuleUriHandler } from "./generateModuleUriHandler";
import { generateEntryUriHandler } from "./generateEntryUriHandler";

export function RouterHapGeneratorTask(
  target: Target,
  node: HvigorNode
): HvigorTask {
  const targetName = target.getTargetName();
  return {
    name: `${targetName}@RouterHapGeneratorTask`,
    run: () => {
      doActionTask(target, node);
    },
    dependencies: [`${target.getTargetName()}@PreBuild`],
    postDependencies: [`${target.getTargetName()}@MergeProfile`],
  };
}

function doActionTask(target: Target,node: HvigorNode) {
  const hapContext = node.getContext(
    OhosPluginId.OHOS_HAP_PLUGIN
  ) as OhosHapContext;

  generateModuleUriHandler(target, hapContext);
  generateEntryUriHandler(target, hapContext);
}