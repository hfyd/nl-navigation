import path from "path";
import FileUtils from "../utils/FileUtils";
import * as fs from "fs";
import Handlebars from "handlebars";
import { parseRouterDescriptorByDecorator } from "./parseRouterDescriptorByDecorator";
import {
  OhosModuleContext,
  Target,
} from "@ohos/hvigor-ohos-plugin/src/plugin/context/plugin-context";
import {
  ImportsDescriptor,
  RouterDescriptor,
  RouterMapJSON,
} from "./interfaces";

export function generateModuleUriHandler(
  target: Target,
  context: OhosModuleContext
): boolean {
  const projectRootPath = context.getModulePath();
  //const descriptor = parseRouterDescriptorByProfile(projectRootPath);
  const descriptor = parseRouterDescriptorByDecorator(projectRootPath);

  if (!descriptor) {
    return false;
  }

  if (descriptor) {
    const buildDir = `${projectRootPath}/build/${target
      .getCurrentProduct()
      .getProductName()}/nlrouter`;
    FileUtils.mkDir(buildDir);
    generatorRouterMap(
      `${buildDir}/router_map.json`,
      transformToRouterMap(context.getModuleName(), projectRootPath, descriptor)
    );

    const generatedDir = `${projectRootPath}/src/main/ets/generated`;
    FileUtils.mkDir(generatedDir);
    generatorModuleRegister(
      projectRootPath,
      `${projectRootPath}/src/main/ets/generated/ModuleUriHandler.ets`,
      descriptor
    );
  }

  return true;
}

function transformToRouterMap(
  moduleName: string,
  projectRootPath: string,
  descriptor: RouterDescriptor
): RouterMapJSON {
  return {
    name: moduleName,
    routerMap: descriptor.pages.map((item) => {
      return {
        name: item.name!,
        pageSourceFile: item.builder!.path!.replace("./", ""),
        buildFunction: item.builder!.name!,
        absolutePath: path.resolve(projectRootPath, item.builder!.path!),
      };
    }),
  };
}

function generatorRouterMap(
  generatorFilePath: string,
  routerMap: RouterMapJSON
) {
  fs.writeFileSync(generatorFilePath, JSON.stringify(routerMap), {
    encoding: "utf8",
  });
}

function generatorModuleRegister(
  rootPath: string,
  generatorFilePath: string,
  descriptor: RouterDescriptor
) {
  const templateTxt = __dirname + "/templates/ModuleUriHandlerBuilder.hbs";
  const output = Handlebars.compile(
    fs.readFileSync(templateTxt, { encoding: "utf8" })
  )({
    imports: parseImports(rootPath, descriptor),
    pages: descriptor.pages,
    services: descriptor.services,
  });

  fs.writeFileSync(generatorFilePath, output, { encoding: "utf8" });
}

function parseImports(
  rootPath: string,
  descriptor: RouterDescriptor
): ImportsDescriptor[] {
  const generatorImports: ImportsDescriptor[] = [];

  function push(item: ImportsDescriptor) {
    if (!generatorImports.find((pre) => pre.name == item.name)) {
      generatorImports.push(item);
    }
  }

  descriptor.pages.forEach(({ interceptors, builder }) => {
    push({
      name: builder!.name,
      isDefault: builder!.isDefault,
      path: getImportPath(rootPath, builder!.path!),
    });
    interceptors?.forEach(({ name, path, isDefault }) => {
      push({
        name,
        isDefault,
        path: getImportPath(rootPath, path!,),
      });
    });
  });

  descriptor.services.forEach(({ imports }) => {
    push({
      name: imports!.name,
      isDefault: imports!.isDefault,
      path: getImportPath(rootPath, imports!.path!),
    });
  });

  return generatorImports;
}

function getImportPath(
  rootPath: string,
  importPath: string,
  filePath: string =  "src/main/ets/generated",
): string {
  if (!importPath.startsWith(".")) {
    // node_modules
    return importPath;
  }

  const profilePath = path.resolve(rootPath, importPath);
  const generatedPath = path.resolve(rootPath, filePath);
  const relativePath = path.relative(generatedPath, profilePath);
  return relativePath.replace(/\.[^/.]+$/, "");
}
