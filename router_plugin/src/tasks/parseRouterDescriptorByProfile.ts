import * as fs from "fs";
import JSON5 from "json5";
import {
  FuncDescriptor,
  PageDescriptor,
  RouterDescriptor,
} from "./generateModuleUriHandler";

interface Page {
  name: string;
  scheme: {
    scheme?: string;
    host?: string;
    path?: string;
  };
  regex: string;
  interceptors: [string] | [{ name?: string; path?: string }];
}

interface Func {
  name: string;
  path: string;
  funcName: string;
}

interface RouterProfile {
  pages: Page[];
  funcs: Func[];
}

export function parseRouterDescriptorByProfile(
  projectRootPath
): RouterDescriptor | null {
  const profile = `${projectRootPath}/router-profile.json5`;
  if (!fs.existsSync(profile)) {
    return null;
  }

  const content = fs.readFileSync(profile);
  if (!content) {
    return null;
  }

  const router = JSON5.parse(content) as RouterProfile;
  const pages = router.pages || [];
  const funcs = router.funcs || [];

  return {
    pages: parsePageDescriptor(pages),
    funcs: parseFuncDescriptor(funcs),
  };
}

function parsePageDescriptor(page: Page[]): PageDescriptor[] {
  return page.map(({ name, scheme, regex, interceptors }) => {
    return {
      name,
      scheme,
      regex,
      interceptors: interceptors?.map((item) => {
        if (typeof item == "string") {
          return {
            path: item,
            name: getFileNameWithoutExtension(item),
            isDefault: true,
          };
        }

        return {
          path: item.path,
          isDefault: false,
          name: item.name,
        };
      }),
    };
  });
}

function getFileNameWithoutExtension(path) {
  // 提取文件名部分
  const fileName = path.split("/").pop();
  // 移除扩展名
  return fileName.replace(/\.[^/.]+$/, "");
}

function parseFuncDescriptor(funcs: Func[]): FuncDescriptor[] {
  return funcs.map((item) => {
    return {
      name: item.name,
      imports: {
        path: item.path,
        isDefault: !item.funcName,
        name: item.funcName ?? item.name,
      },
    } as FuncDescriptor;
  });
}
