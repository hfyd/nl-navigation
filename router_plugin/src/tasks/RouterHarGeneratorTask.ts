import { HvigorNode, HvigorTask } from "@ohos/hvigor";
import { Target, OhosPluginId, OhosHarContext } from "@ohos/hvigor-ohos-plugin";
import { generateModuleUriHandler } from "./generateModuleUriHandler";

export function RouterHarGeneratorTask(
  target: Target,
  node: HvigorNode
): HvigorTask {
  const targetName = target.getTargetName();
  return {
    name: `${targetName}@RouterHarGeneratorTask`,
    run: () => {
      doTaskAction(target, node);
    },
    postDependencies: [`${target.getTargetName()}@PreBuild`],
  };
}

function doTaskAction(target: Target, node: HvigorNode) {
  const harContext = node.getContext(
    OhosPluginId.OHOS_HAR_PLUGIN
  ) as OhosHarContext;

  generateModuleUriHandler(target, harContext);
}

