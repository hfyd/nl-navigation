import { HvigorNode, HvigorTask } from "@ohos/hvigor";
import { OhosPluginId, Target } from "@ohos/hvigor-ohos-plugin";
import FileUtils from "../utils/FileUtils";
import { OhosModuleContext } from "@ohos/hvigor-ohos-plugin/src/plugin/context/plugin-context";

export function CleanTask(node: HvigorNode): HvigorTask {
  return {
    name: `GeneratedClean`,
    run: () => {
      FileUtils.deleteDir(getCleanPath(node));
    },
    postDependencies: [`clean`],
  };
}

function getCleanPath(node: HvigorNode): string {
  let context = (node.getContext(OhosPluginId.OHOS_HAR_PLUGIN) ||
    node.getContext(OhosPluginId.OHOS_HAP_PLUGIN)) as OhosModuleContext;
  const rootPath = context.getModulePath();
  return `${rootPath}/src/main/ets/generated/`
}
