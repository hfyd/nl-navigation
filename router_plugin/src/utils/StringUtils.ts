export default class StringUtils {
  static toCamelCase(input: string): string {
    const camelCase = input
      .toLowerCase()
      .replace(/[-_](.)/g, (_, char) => char.toUpperCase());
    return camelCase.charAt(0).toUpperCase() + camelCase.slice(1);
  }
}
