import { HvigorNode, HvigorPlugin } from "@ohos/hvigor";
import { OhosHarContext, OhosPluginId, Target } from "@ohos/hvigor-ohos-plugin";
import { RouterHarGeneratorTask } from "./tasks/RouterHarGeneratorTask";
import { CleanTask } from "./tasks/CleanTask";

interface Config {
  name: string;
}

export function RouterLibraryPlugin(options: Config): HvigorPlugin {
  return {
    pluginId: "RouterLibraryPlugin",
    apply(node: HvigorNode) {
      const harContext = node.getContext(
        OhosPluginId.OHOS_HAR_PLUGIN
      ) as OhosHarContext;
      harContext?.targets((target: Target) => {
        node.registerTask(CleanTask(node));
        node.registerTask(RouterHarGeneratorTask(target, node));
      });
    },
  };
}
