import { HvigorNode, HvigorPlugin } from '@ohos/hvigor';
import { OhosHapContext, OhosPluginId, Target } from '@ohos/hvigor-ohos-plugin';
import { RouterHapGeneratorTask } from './tasks/RouterHapGeneratorTask';
import { CleanTask } from './tasks/CleanTask';

export function RouterAppPlugin(hvigor): HvigorPlugin {
  return {
    pluginId: 'RouterAppPlugin',
    apply: async () => {
      hvigor.nodesEvaluated(() => {
        registerRouterGeneratorTask(hvigor);
      })
    }
  }
}

function registerRouterGeneratorTask(hvigor) {
  hvigor.getRootNode().subNodes((node: HvigorNode) => {
    const hapContext = node.getContext(OhosPluginId.OHOS_HAP_PLUGIN) as OhosHapContext;
    hapContext?.targets((target: Target) => {
      node.registerTask(CleanTask(node));
      node.registerTask(RouterHapGeneratorTask(target, node));
    });
  });
}
